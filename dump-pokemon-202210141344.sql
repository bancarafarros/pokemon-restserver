-- MySQL dump 10.13  Distrib 5.7.33, for Win64 (x86_64)
--
-- Host: localhost    Database: pokemon
-- ------------------------------------------------------
-- Server version	5.7.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pokemon_generation`
--

DROP TABLE IF EXISTS `pokemon_generation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pokemon_generation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `generation` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `generation_un` (`generation`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pokemon_generation`
--

LOCK TABLES `pokemon_generation` WRITE;
/*!40000 ALTER TABLE `pokemon_generation` DISABLE KEYS */;
INSERT INTO `pokemon_generation` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,11);
/*!40000 ALTER TABLE `pokemon_generation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pokemon_list`
--

DROP TABLE IF EXISTS `pokemon_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pokemon_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `max_cp` int(11) NOT NULL,
  `attack` int(11) NOT NULL,
  `defense` int(11) NOT NULL,
  `stamina` int(11) NOT NULL,
  `generation` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pokemon_list_un` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pokemon_list`
--

LOCK TABLES `pokemon_list` WRITE;
/*!40000 ALTER TABLE `pokemon_list` DISABLE KEYS */;
INSERT INTO `pokemon_list` VALUES (1,'Caterpie','Bug',494,55,55,128,1),(2,'Metapod','Bug',509,45,80,137,1),(3,'Beedrill','Bug, Poison',2087,169,130,163,1),(4,'Scyther','Bug, Flying',3060,218,170,172,1),(5,'Scizor','Bug, Steel',3393,236,181,172,2),(6,'Rayquaza','Dragon, Flying',4336,284,170,213,3),(7,'Dialga','Steel, Dragon',4565,275,211,205,4),(8,'Arceus','Fairy',4510,238,238,237,4),(9,'Entei','Fire',3926,235,171,251,2),(10,'Giratina','Ghost, Dragon',3820,187,225,284,4),(11,'Darkrai','Dark',4227,285,198,172,4),(12,'Xurkitree','Electric',4451,330,144,195,7),(13,'Terrakion','Rock, Fighting',4181,260,192,260,5),(14,'Celebi','Psychic, Grass',3691,210,210,225,2),(15,'Groudon','Ground',4652,270,228,205,3),(16,'Regice','Ice',3530,179,309,190,3),(17,'Bouffalant','Normal',3163,195,182,216,5),(18,'Kyogre','Water',4652,270,228,205,3);
/*!40000 ALTER TABLE `pokemon_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pokemon_type`
--

DROP TABLE IF EXISTS `pokemon_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pokemon_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pokemon_type_un` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pokemon_type`
--

LOCK TABLES `pokemon_type` WRITE;
/*!40000 ALTER TABLE `pokemon_type` DISABLE KEYS */;
INSERT INTO `pokemon_type` VALUES (1,'Bug'),(19,'CCCC'),(2,'Dark'),(3,'Dragon'),(4,'Electric'),(5,'Fairy'),(6,'Fighting'),(7,'Fire'),(8,'Flying'),(9,'Ghost'),(10,'Grass'),(11,'Ground'),(12,'Ice'),(13,'Normal'),(14,'Poison'),(15,'Psychic'),(16,'Rock'),(17,'Steel'),(18,'Water');
/*!40000 ALTER TABLE `pokemon_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'pokemon'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-14 13:44:28
