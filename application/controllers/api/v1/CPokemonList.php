<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class CPokemonList extends REST_Controller {
    
    // public function index_get() {
    //     $id = $this->get('id');
    //     $name = $this->get('name');
    //     $type = $this->get('type');
    //     $max_cp = $this->get('max_cp');
    //     $attack = $this->get('attack');
    //     $defense = $this->get('defense');
    //     $stamina = $this->get('stamina');
    //     $generation = $this->get('generation');

    //     // if ($id == null) {
    //     //     $pokemon = $this->MPokemonList->tampilData()->result();
        
    //     // } else {
    //     //     $pokemon = $this->MPokemonList->tampilData($id)->result();
    //     // }

    //     if ($id == null && $name == null && $type == null && $max_cp == null && $attack == null && $defense == null && $stamina == null && $generation == null) {
    //         $pokemon = $this->MPokemonList->tampilData()->result();
        
    //     } elseif ($id != null) {
    //         $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation)->result();
        
    //     } elseif ($name != null) {
    //         $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation)->result();
        
    //     } elseif ($type != null) {
    //         $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation)->result();
        
    //     } elseif ($max_cp != null) {
    //         $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation)->result();
        
    //     } elseif ($attack != null) {
    //         $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation)->result();
        
    //     } elseif ($defense != null) {
    //         $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation)->result();
        
    //     } elseif ($stamina != null) {
    //         $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation)->result();
        
    //     } elseif ($generation != null) {
    //         $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation)->result();
    //     }
        
    //     if ($pokemon) {
    //         $this->response([
    //             'status' => TRUE,
    //             'message' => 'Berhasil mint, datanya ada',
    //             'data' => $pokemon
    //         ], REST_Controller::HTTP_OK);
        
    //     } else {
    //         $this->response([
    //             'status' => FALSE,
    //             'message' => 'Gk berhasil mint, datanya gk ada',
    //         ], REST_Controller::HTTP_NOT_FOUND);
    //     }
    // }

    public function index_get() {
        $id = $this->get('id');
        $name = $this->get('name');
        $type = $this->get('type');
        $max_cp = $this->get('max_cp');
        $attack = $this->get('attack');
        $defense = $this->get('defense');
        $stamina = $this->get('stamina');
        $generation = $this->get('generation');
        $orderBy = $this->get('orderBy');

        if ($id == null && $name == null && $type == null && $max_cp == null && $attack == null && $defense == null && $stamina == null && $generation == null && $orderBy == null) {
            $pokemon = $this->MPokemonList->tampilData()->result();
        
        } elseif ($id != null) {
            $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
        
        } elseif ($name != null) {
            $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
        
        } elseif ($type != null) {
            $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
        
        } elseif ($max_cp != null) {
            $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
        
        } elseif ($attack != null) {
            $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
        
        } elseif ($defense != null) {
            $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
        
        } elseif ($stamina != null) {
            $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
        
        } elseif ($generation != null) {
            $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
        
        } else {
            if ($orderBy == "name") {
                $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
            
            } elseif ($orderBy == "type") {
                $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
            
            }  elseif ($orderBy == "max_cp") {
                $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
            
            }  elseif ($orderBy == "attack") {
                $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
            
            }  elseif ($orderBy == "defense") {
                $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
            
            }  elseif ($orderBy == "stamina") {
                $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
            
            }  elseif ($orderBy == "generation") {
                $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
            
            } else {
                if ($orderBy == "name-desc") {
                    $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
                
                } elseif ($orderBy == "type-desc") {
                    $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
                
                }  elseif ($orderBy == "max_cp-desc") {
                    $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
                
                }  elseif ($orderBy == "attack-desc") {
                    $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
                
                }  elseif ($orderBy == "defense-desc") {
                    $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
                
                }  elseif ($orderBy == "stamina-desc") {
                    $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
                
                }  elseif ($orderBy == "generation-desc") {
                    $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation, $orderBy)->result();
                }
            }
        }
        
        if ($pokemon) {
            $this->response([
                'status' => TRUE,
                'message' => 'Berhasil mint, datanya ada',
                'data' => $pokemon
            ], REST_Controller::HTTP_OK);
        
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Gk berhasil mint, datanya gk ada',
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    // public function orderByName_get() {
    //     $id = $this->get('id');
    //     $name = $this->get('name');
    //     $type = $this->get('type');
    //     $max_cp = $this->get('max_cp');
    //     $attack = $this->get('attack');
    //     $defense = $this->get('defense');
    //     $stamina = $this->get('stamina');
    //     $generation = $this->get('generation');

    //     if ($id == null && $name == null && $type == null && $max_cp == null && $attack == null && $defense == null && $stamina == null && $generation == null) {
    //         $pokemon = $this->MPokemonList->tampilData()->result();
        
    //     } elseif ($id != null) {
    //         $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation)->result();
        
    //     }  elseif ($name != null) {
    //         $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation)->result();
        
    //     }  elseif ($type != null) {
    //         $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation)->result();
        
    //     }  elseif ($max_cp != null) {
    //         $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation)->result();
        
    //     }  elseif ($attack != null) {
    //         $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation)->result();
        
    //     }  elseif ($defense != null) {
    //         $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation)->result();
        
    //     }  elseif ($stamina != null) {
    //         $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation)->result();
        
    //     }  elseif ($generation != null) {
    //         $pokemon = $this->MPokemonList->tampilData($id, $name, $type, $max_cp, $attack, $defense, $stamina, $generation)->result();
    //     }
        
    //     if ($pokemon) {
    //         $this->response([
    //             'status' => TRUE,
    //             'message' => 'Berhasil mint, datanya ada',
    //             'data' => $pokemon
    //         ], REST_Controller::HTTP_OK);
        
    //     } else {
    //         $this->response([
    //             'status' => FALSE,
    //             'message' => 'Gk berhasil mint, datanya gk ada',
    //         ], REST_Controller::HTTP_NOT_FOUND);
    //     }
    // }

    public function index_post() {
        $dataPost = [
            'name' => $this->post('name'),
            'type' => $this->post('type'),
            'max_cp' => $this->post('max_cp'),
            'attack' => $this->post('attack'),
            'defense' => $this->post('defense'),
            'stamina' => $this->post('stamina'),
            'generation' => $this->post('generation'),
        ];

        if ($this->MPokemonList->fungsiTambah($dataPost) > 0) {
            $this->response([
                'status' => TRUE,
                'message' => 'Berhasil mint, datanya masuk',
            ], REST_Controller::HTTP_CREATED);
        
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Gk bisa mint, datanya gk masuk',
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_put() {
        $id = $this->put('id');
        $dataPut = [
            'name' => $this->put('name'),
            'type' => $this->put('type'),
            'max_cp' => $this->put('max_cp'),
            'attack' => $this->put('attack'),
            'defense' => $this->put('defense'),
            'stamina' => $this->put('stamina'),
            'generation' => $this->put('generation'),
        ];

        if ($this->MPokemonList->fungsiUpdate($dataPut, $id) > 0) {
            $this->response([
                'status' => TRUE,
                'message' => 'Berhasil mint, datanya udah diubah',
            ], REST_Controller::HTTP_ACCEPTED);
        
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Gk bisa mint, datanya gk bisa diubah',
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_delete() {
        $id = $this->delete('id');

        if ($id == null) {
            $this->response([
                'status' => FALSE,
                'message' => 'Gk dulu mint, besok2 lagi ya',
            ], REST_Controller::HTTP_BAD_REQUEST);
        
        } else {
            if ($this->MPokemonList->fungsiDelete($id) > 0) {
                $this->response([
                    'status' => TRUE,
                    'id' => $id,
                    'message' => 'Berhasil mint, datanya kehapus',
                ], REST_Controller::HTTP_ACCEPTED);
            
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Blom bisa mint, idnya gk ada',
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }
}

?>