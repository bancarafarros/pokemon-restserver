<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class CPokemonType extends REST_Controller {
    
    public function index_get() {
        $id = $this->get('id');
        $type = $this->get('type');
        $orderBy = $this->get('orderBy');

        if ($id == null && $type == null && $orderBy == null) {
            $pokemon = $this->MPokemonType->tampilData()->result();
        
        } elseif ($id != null) {
            $pokemon = $this->MPokemonType->tampilData($id, $type, $orderBy)->result();
        
        } elseif ($type != null) {
            $pokemon = $this->MPokemonType->tampilData($id, $type, $orderBy)->result();
        
        } else {
            if ($orderBy == "type") {
                $pokemon = $this->MPokemonType->tampilData($id, $type, $orderBy)->result();
            
            } else {
                if ($orderBy == "type-desc") {
                    $pokemon = $this->MPokemonType->tampilData($id, $type, $orderBy)->result();
                }
            }
        }

        if ($pokemon) {
            $this->response([
                'status' => TRUE,
                'message' => 'Berhasil mint, datanya ada',
                'data' => $pokemon
            ], REST_Controller::HTTP_OK);
        
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Gk berhasil mint, datanya gk ada',
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function index_post() {
        $dataPost = [
            'type' => $this->post('type')
        ];

        if ($this->MPokemonType->fungsiTambah($dataPost) > 0) {
            $this->response([
                'status' => TRUE,
                'message' => 'Berhasil mint, datanya masuk',
            ], REST_Controller::HTTP_CREATED);
        
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Gk bisa mint, datanya gk masuk',
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_put() {
        $id = $this->put('id');
        $dataPut = [
            'type' => $this->put('type')
        ];

        if ($this->MPokemonType->fungsiUpdate($dataPut, $id) > 0) {
            $this->response([
                'status' => TRUE,
                'message' => 'Berhasil mint, datanya udah diubah',
            ], REST_Controller::HTTP_ACCEPTED);
        
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Gk bisa mint, datanya gk bisa diubah',
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_delete() {
        $id = $this->delete('id');

        if ($id == null) {
            $this->response([
                'status' => FALSE,
                'message' => 'Gk dulu mint, besok2 lagi ya',
            ], REST_Controller::HTTP_BAD_REQUEST);
        
        } else {
            if ($this->MPokemonType->fungsiDelete($id) > 0) {
                $this->response([
                    'status' => TRUE,
                    'id' => $id,
                    'message' => 'Berhasil mint, datanya kehapus',
                ], REST_Controller::HTTP_ACCEPTED);
            
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Blom bisa mint, idnya gk ada',
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }
}

?>