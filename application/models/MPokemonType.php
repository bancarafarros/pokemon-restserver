<?php

class MPokemonType extends CI_Model {
    
    // public function tampilData($id = null) {
    //     if ($id == null) {
    //         return $this->db->get('pokemon_type');
        
    //     } else {
    //         return $this->db->get_where('pokemon_type', ['id' => $id]);
    //     }
    // }

    public function tampilData($id = null, $type = null, $orderBy = null) {
        if ($id == null && $type == null && $orderBy == null) {
            return $this->db->get('pokemon_type');
        
        } elseif ($id != null) {
            return $this->db->get_where('pokemon_type', ['id' => $id]);
        
        } elseif ($type != null) {
            return $this->db->get_where('pokemon_type', ['generation' => $type]);
        
        } else {
            if ($orderBy == "type") {
                $this->db->order_by("type asc");
                return $this->db->get('pokemon_type');
            
            } else {
                if ($orderBy == "type-desc") {
                    $this->db->order_by("type desc");
                    return $this->db->get('pokemon_type');
                }
            }    
        }
    }

    public function fungsiTambah($dataPost) {
        $this->db->insert('pokemon_type', $dataPost);
        return $this->db->affected_rows();
    }

    public function fungsiUpdate($dataPut, $id) {
		$this->db->update('pokemon_type', $dataPut, ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function fungsiDelete($id) {
        $this->db->delete('pokemon_type', ['id' => $id]);
        return $this->db->affected_rows();
    }
}

?>