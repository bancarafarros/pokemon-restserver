<?php

class MPokemonGeneration extends CI_Model {
    
    // public function tampilData($id = null) {
    //     if ($id == null) {
    //         return $this->db->get('pokemon_generation');
        
    //     } else {
    //         return $this->db->get_where('pokemon_generation', ['id' => $id]);
    //     }
    // }

    public function tampilData($id = null, $generation = null, $orderBy = null) {
        if ($id == null && $generation == null && $orderBy == null) {
            return $this->db->get('pokemon_generation');
        
        } elseif ($id != null) {
            return $this->db->get_where('pokemon_generation', ['id' => $id]);
        
        } elseif ($generation != null) {
            return $this->db->get_where('pokemon_generation', ['generation' => $generation]);
        
        } else {
            if ($orderBy == "generation") {
                $this->db->order_by("generation asc");
                return $this->db->get('pokemon_generation');
            
            } else {
                if ($orderBy == "generation-desc") {
                    $this->db->order_by("generation desc");
                    return $this->db->get('pokemon_generation');
                }
            }    
        }
    }

    public function fungsiTambah($dataPost) {
        $this->db->insert('pokemon_generation', $dataPost);
        return $this->db->affected_rows();
    }

    public function fungsiUpdate($dataPut, $id) {
		$this->db->update('pokemon_generation', $dataPut, ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function fungsiDelete($id) {
        $this->db->delete('pokemon_generation', ['id' => $id]);
        return $this->db->affected_rows();
    }
}

?>