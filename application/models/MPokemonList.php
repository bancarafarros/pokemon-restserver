<?php

class MPokemonList extends CI_Model {
    
    // public function tampilData($id = null) {
    //     if ($id == null) {
    //         return $this->db->get('pokemon_list');
        
    //     } else {
    //         return $this->db->get_where('pokemon_list', ['id' => $id]);
    //     }
    // }

    // public function tampilData($id = null, $name = null, $type = null, $max_cp = null, $attack = null, $defense = null, $stamina = null, $generation = null) {
    //     if ($id == null && $name == null && $type == null && $max_cp == null && $attack == null && $defense == null && $stamina == null && $generation == null) {
    //         // $this->db->order_by("name asc");
    //         return $this->db->get('pokemon_list');
        
    //     } elseif ($id != null) {
    //         return $this->db->get_where('pokemon_list', ['id' => $id]);
        
    //     }  elseif ($name != null) {
    //         return $this->db->get_where('pokemon_list', ['name' => $name]);
        
    //     }  elseif ($type != null) {
    //         return $this->db->get_where('pokemon_list', ['type' => $type]);
        
    //     }  elseif ($max_cp != null) {
    //         return $this->db->get_where('pokemon_list', ['max_cp' => $max_cp]);
        
    //     }  elseif ($attack != null) {
    //         return $this->db->get_where('pokemon_list', ['attack' => $attack]);
        
    //     }  elseif ($defense != null) {
    //         return $this->db->get_where('pokemon_list', ['defense' => $defense]);
        
    //     }  elseif ($stamina != null) {
    //         return $this->db->get_where('pokemon_list', ['stamina' => $stamina]);
        
    //     }  elseif ($generation != null) {
    //         return $this->db->get_where('pokemon_list', ['generation' => $generation]);
    //     }
    // }

    public function tampilData($id = null, $name = null, $type = null, $max_cp = null, $attack = null, $defense = null, $stamina = null, $generation = null, $orderBy = null) {
        if ($id == null && $name == null && $type == null && $max_cp == null && $attack == null && $defense == null && $stamina == null && $generation == null && $orderBy == null) {
            return $this->db->get('pokemon_list');
        
        } elseif ($id != null) {
            return $this->db->get_where('pokemon_list', ['id' => $id]);
        
        } elseif ($name != null) {
            return $this->db->get_where('pokemon_list', ['name' => $name]);
        
        } elseif ($type != null) {
            return $this->db->get_where('pokemon_list', ['type' => $type]);
        
        } elseif ($max_cp != null) {
            return $this->db->get_where('pokemon_list', ['max_cp' => $max_cp]);
        
        } elseif ($attack != null) {
            return $this->db->get_where('pokemon_list', ['attack' => $attack]);
        
        } elseif ($defense != null) {
            return $this->db->get_where('pokemon_list', ['defense' => $defense]);
        
        } elseif ($stamina != null) {
            return $this->db->get_where('pokemon_list', ['stamina' => $stamina]);
        
        } elseif ($generation != null) {
            return $this->db->get_where('pokemon_list', ['generation' => $generation]);
        
        } else {
            if ($orderBy == "name") {
                $this->db->order_by("name asc");
                return $this->db->get('pokemon_list');
            
            } elseif ($orderBy == "type") {
                $this->db->order_by("type asc");
                return $this->db->get('pokemon_list');
            
            } elseif ($orderBy == "max_cp") {
                $this->db->order_by("max_cp asc");
                return $this->db->get('pokemon_list');
            
            } elseif ($orderBy == "attack") {
                $this->db->order_by("attack asc");
                return $this->db->get('pokemon_list');
            
            } elseif ($orderBy == "defense") {
                $this->db->order_by("defense asc");
                return $this->db->get('pokemon_list');
            
            } elseif ($orderBy == "stamina") {
                $this->db->order_by("stamina asc");
                return $this->db->get('pokemon_list');
            
            } elseif ($orderBy == "generation") {
                $this->db->order_by("generation asc");
                return $this->db->get('pokemon_list');
            
            } else {
                if ($orderBy == "name-desc") {
                    $this->db->order_by("name desc");
                    return $this->db->get('pokemon_list');
                
                } elseif ($orderBy == "type-desc") {
                    $this->db->order_by("type desc");
                    return $this->db->get('pokemon_list');
                
                } elseif ($orderBy == "max_cp-desc") {
                    $this->db->order_by("max_cp desc");
                    return $this->db->get('pokemon_list');
                
                } elseif ($orderBy == "attack-desc") {
                    $this->db->order_by("attack desc");
                    return $this->db->get('pokemon_list');
                
                } elseif ($orderBy == "defense-desc") {
                    $this->db->order_by("defense desc");
                    return $this->db->get('pokemon_list');
                
                } elseif ($orderBy == "stamina-desc") {
                    $this->db->order_by("stamina desc");
                    return $this->db->get('pokemon_list');
                
                } elseif ($orderBy == "generation-desc") {
                    $this->db->order_by("generation desc");
                    return $this->db->get('pokemon_list');
                }
            }    
        }
    }

    // public function orderByName($id = null, $name = null, $type = null, $max_cp = null, $attack = null, $defense = null, $stamina = null, $generation = null) {
    //     if ($id == null && $name == null && $type == null && $max_cp == null && $attack == null && $defense == null && $stamina == null && $generation == null) {
    //         $this->db->order_by("name asc");
    //         return $this->db->get('pokemon_list');
        
    //     } elseif ($id != null) {
    //         return $this->db->get_where('pokemon_list', ['id' => $id]);
        
    //     }  elseif ($name != null) {
    //         return $this->db->get_where('pokemon_list', ['name' => $name]);
        
    //     }  elseif ($type != null) {
    //         return $this->db->get_where('pokemon_list', ['type' => $type]);
        
    //     }  elseif ($max_cp != null) {
    //         return $this->db->get_where('pokemon_list', ['max_cp' => $max_cp]);
        
    //     }  elseif ($attack != null) {
    //         return $this->db->get_where('pokemon_list', ['attack' => $attack]);
        
    //     }  elseif ($defense != null) {
    //         return $this->db->get_where('pokemon_list', ['defense' => $defense]);
        
    //     }  elseif ($stamina != null) {
    //         return $this->db->get_where('pokemon_list', ['stamina' => $stamina]);
        
    //     }  elseif ($generation != null) {
    //         return $this->db->get_where('pokemon_list', ['generation' => $generation]);
    //     } 
    // }

    public function fungsiTambah($dataPost) {
        $this->db->insert('pokemon_list', $dataPost);
        return $this->db->affected_rows();
    }

    public function fungsiUpdate($dataPut, $id) {
		$this->db->update('pokemon_list', $dataPut, ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function fungsiDelete($id) {
        $this->db->delete('pokemon_list', ['id' => $id]);
        return $this->db->affected_rows();
    }
}

?>